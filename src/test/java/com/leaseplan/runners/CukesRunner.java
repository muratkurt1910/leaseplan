package com.leaseplan.runners;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {
                "pretty",
                "json:target/cucumber.json", "html:target/default-html-reports", "rerun:target/rerun.txt"
        },

        features = "src/test/resources/features",
        glue = "com/leaseplan/step_definitions",
        dryRun = false,
        tags = ""
)

public class CukesRunner {
}
