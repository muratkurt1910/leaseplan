package com.leaseplan.step_definitions.lease;


import com.google.gson.Gson;
import com.leaseplan.pojos.pets.Pets;
import com.leaseplan.utils.ConfigurationReader;
import com.sun.net.httpserver.HttpServer;
import cucumber.api.java.en.*;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.protocol.HTTP;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.junit.Assert.assertEquals;


public class leasePlanStepDef {

    RequestSpecification requestSpecification;
    Response response;
    String baseURI = ConfigurationReader.get("baseURI");

    @Given("Headers accepts content type as {string}")
    public void headers_accepts_content_type_as(String contentType) {
        requestSpecification = given().accept(contentType).contentType(ContentType.JSON);
    }

    @When("User sends GET request to {string}{int}")
    public void user_sends_GET_request_to(String path, int id) {
        response = when().get(baseURI+path+id);
    }

    @Then("User verifies that response status code is {int}")
    public void user_verifies_that_response_status_code_is(int statusCode) {
        System.out.println("statusCode = " + statusCode);
        System.out.println("response.statusCode() = " + response.statusCode());
        assertEquals(statusCode, response.statusCode());
    }

    @Then("User verifies that response content type is {string}")
    public void user_verifies_that_response_content_type_is(String contentType) {
        assertEquals(contentType, response.contentType());
    }

    @When("User sends POST request to {string}")
    public void user_sends_POST_request_to(String path, Map<String,String> pets) {
        System.out.println("pets = " + pets);

        Pets pet = new Pets();
        pet.setId(Integer.parseInt(pets.get("id")));
        pet.setName(pets.get("name"));
        pet.setStatus(pets.get("status"));

        System.out.println("path = " + baseURI+path);
        response = requestSpecification.body(pet.toString()).when().put(baseURI+path);
    }

}
