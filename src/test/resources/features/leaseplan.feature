Feature: Leaseplan API Test

  Scenario Outline: User should be able to reach pet information by id
    Given Headers accepts content type as "application/json"
    When User sends GET request to "pet/"<id>
    Then User verifies that response status code is <statusCode>
    And User verifies that response content type is "<contentType>"

    Examples:
      | id   | statusCode | contentType      |
      | 1245 | 200        | application/json |
      | 37   | 404        | application/json |
      | 100  | 200        | application/json |

  Scenario Outline: User should be able to create a pet by given information
    Given Headers accepts content type as "application/json"
    When User sends POST request to "pet"
      | id     | <id>     |
      | name   | <name>   |
      | status | <status> |
    Then User verifies that response status code is 200
    And User verifies that response content type is "application/json"

    Examples:
      | id   | name  | status    |
      | 5574 | reco  | available |
      | 9976 | teyyo | pending   |
      | 9056 | erdo  | sold      |

