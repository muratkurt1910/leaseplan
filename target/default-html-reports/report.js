$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/leaseplan.feature");
formatter.feature({
  "name": "Leaseplan API Test",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "User should be able to reach pet information by id",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.step({
  "name": "User sends GET request to \"pet/\"\u003cid\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "User verifies that response status code is \u003cstatusCode\u003e",
  "keyword": "Then "
});
formatter.step({
  "name": "User verifies that response content type is \"\u003ccontentType\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "id",
        "statusCode",
        "contentType"
      ]
    },
    {
      "cells": [
        "1245",
        "200",
        "application/json"
      ]
    },
    {
      "cells": [
        "37",
        "404",
        "application/json"
      ]
    },
    {
      "cells": [
        "100",
        "200",
        "application/json"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User should be able to reach pet information by id",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "leasePlanStepDef.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends GET request to \"pet/\"1245",
  "keyword": "When "
});
formatter.match({
  "location": "leasePlanStepDef.user_sends_GET_request_to(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.match({
  "location": "leasePlanStepDef.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.match({
  "location": "leasePlanStepDef.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User should be able to reach pet information by id",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "leasePlanStepDef.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends GET request to \"pet/\"37",
  "keyword": "When "
});
formatter.match({
  "location": "leasePlanStepDef.user_sends_GET_request_to(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 404",
  "keyword": "Then "
});
formatter.match({
  "location": "leasePlanStepDef.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.match({
  "location": "leasePlanStepDef.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User should be able to reach pet information by id",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "leasePlanStepDef.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends GET request to \"pet/\"100",
  "keyword": "When "
});
formatter.match({
  "location": "leasePlanStepDef.user_sends_GET_request_to(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.match({
  "location": "leasePlanStepDef.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.match({
  "location": "leasePlanStepDef.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "User should be able to create a pet by given information",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.step({
  "name": "User sends POST request to \"pet\"",
  "keyword": "When ",
  "rows": [
    {
      "cells": [
        "id",
        "\u003cid\u003e"
      ]
    },
    {
      "cells": [
        "name",
        "\u003cname\u003e"
      ]
    },
    {
      "cells": [
        "status",
        "\u003cstatus\u003e"
      ]
    }
  ]
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "id",
        "name",
        "status"
      ]
    },
    {
      "cells": [
        "5574",
        "reco",
        "available"
      ]
    },
    {
      "cells": [
        "9976",
        "teyyo",
        "pending"
      ]
    },
    {
      "cells": [
        "9056",
        "erdo",
        "sold"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User should be able to create a pet by given information",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "leasePlanStepDef.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends POST request to \"pet\"",
  "rows": [
    {
      "cells": [
        "id",
        "5574"
      ]
    },
    {
      "cells": [
        "name",
        "reco"
      ]
    },
    {
      "cells": [
        "status",
        "available"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "leasePlanStepDef.user_sends_POST_request_to(String,String,String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.match({
  "location": "leasePlanStepDef.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.match({
  "location": "leasePlanStepDef.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User should be able to create a pet by given information",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "leasePlanStepDef.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends POST request to \"pet\"",
  "rows": [
    {
      "cells": [
        "id",
        "9976"
      ]
    },
    {
      "cells": [
        "name",
        "teyyo"
      ]
    },
    {
      "cells": [
        "status",
        "pending"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "leasePlanStepDef.user_sends_POST_request_to(String,String,String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.match({
  "location": "leasePlanStepDef.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.match({
  "location": "leasePlanStepDef.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User should be able to create a pet by given information",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "leasePlanStepDef.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends POST request to \"pet\"",
  "rows": [
    {
      "cells": [
        "id",
        "9056"
      ]
    },
    {
      "cells": [
        "name",
        "erdo"
      ]
    },
    {
      "cells": [
        "status",
        "sold"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "leasePlanStepDef.user_sends_POST_request_to(String,String,String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.match({
  "location": "leasePlanStepDef.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.match({
  "location": "leasePlanStepDef.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
});